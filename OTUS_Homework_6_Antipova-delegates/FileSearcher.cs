﻿namespace OTUS_Homework_6_Antipova_delegates;

public class FileSearcher
{
    public event EventHandler FileFound;

    private readonly string _startDirectory;
    private Task _worker;

    public FileSearcher(string startDirectory)
    {
        _startDirectory = startDirectory;
    }

    public Task Start(CancellationToken cancellationToken)
    {
        if (_worker == null || _worker?.Status != TaskStatus.Running)
        {
            _worker = Task.Run(() => { ScanFolder(_startDirectory, cancellationToken); }, cancellationToken);
        }
        return _worker;
    }

    private void ScanFolder(string path, CancellationToken cancellationToken)
    {
        Thread.Sleep(200);
        if (!Directory.Exists(path) || cancellationToken.IsCancellationRequested)
            return;

        try
        {
            var files = Directory.GetFiles(path);
            foreach (var f in files)
            {
                var newFile = new FileArgs()
                {
                    Name = Path.GetFileName(f),
                    Path = f
                };
                FileFound?.Invoke(this, newFile);

            }

            var dir = Directory.GetDirectories(path);
            foreach (var d in dir)
                ScanFolder(d, cancellationToken);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public class FileArgs : EventArgs
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}


