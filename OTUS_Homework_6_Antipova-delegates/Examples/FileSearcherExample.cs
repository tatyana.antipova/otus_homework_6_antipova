﻿namespace OTUS_Homework_6_Antipova_delegates.Examples;

internal static class FileSearcherExample
{
    public static void Run()
    { 
        var result = new List<FileSearcher.FileArgs>();

        var scanner = new FileSearcher(@"C:\");
        scanner.FileFound += (s, e) => {
            var args = (FileSearcher.FileArgs)e;
            Console.WriteLine(args.Name);
        };

        CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        CancellationToken cancellationToken = cancelTokenSource.Token;

        scanner.Start(cancellationToken);
        Thread.Sleep(2500);
        cancelTokenSource.Cancel();

        Console.WriteLine("Task cancelled");

    }
}
