﻿using System.Data;

namespace OTUS_Homework_6_Antipova_delegates.Examples;

internal static class GetMaxExample
{    
    public static GetMaxData Run()
    {
        var dataset = new List<GetMaxData>()
        {
            new GetMaxData() { Id = 1, ValueProperty = 0 },
            new GetMaxData() { Id = 2, ValueProperty = 1 },
            new GetMaxData() { Id = 3, ValueProperty = 1 },
            new GetMaxData() { Id = 4, ValueProperty = 2 },
            new GetMaxData() { Id = 5, ValueProperty = 3 },
            new GetMaxData() { Id = 6, ValueProperty = 5 }
        };

        return dataset.GetMax<GetMaxData>(x => x.ValueProperty);
    }

    public class GetMaxData
    {
        public int Id { get; set; }
        public float ValueProperty { get; set; }
    }
}
