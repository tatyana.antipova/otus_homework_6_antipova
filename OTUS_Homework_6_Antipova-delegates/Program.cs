﻿using OTUS_Homework_6_Antipova_delegates.Examples;

namespace OTUS_Homework_6_Antipova_delegates
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var example1Result = GetMaxExample.Run();
            Console.WriteLine($"example1Result: Id={example1Result.Id}, ValueProperty={example1Result.ValueProperty}");
            
            FileSearcherExample.Run();            

            Console.ReadLine();
        }
    }
}