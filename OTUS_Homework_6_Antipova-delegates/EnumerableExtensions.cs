﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace OTUS_Homework_6_Antipova_delegates;

internal static class EnumerableExtensions
{
    public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
    {
        T? maxItem = null;
        foreach (var item in e)
        {
            if (maxItem == null || getParameter(maxItem) < getParameter((T)item))
                maxItem = (T)item;
        }
        return (T)maxItem;
    }

}
